//*****************************************************************************
//
// Copyright (C) 2015 - 2016 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//  Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//
//  Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the
//  distribution.
//
//  Neither the name of Texas Instruments Incorporated nor the names of
//  its contributors may be used to endorse or promote products derived
//  from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

// Includes standard
#include <stdio.h>
#include <stdint.h>
#include <string.h>

// Includes FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

// Includes drivers
#include "i2c_driver.h"
#include "temp_sensor.h"
#include "opt3001.h"
#include "tmp006.h"
#include <ti/grlib/grlib.h>
#include "LcdDriver/Crystalfontz128x128_ST7735.h"
#include "accelerometer_driver.h"
#include "adc14_multiple_channel_no_repeat.h"

// Definicion de parametros RTOS
#define WRITER_TASK_PRIORITY    1
#define READER_TASK_PRIORITY  2
#define HEARTBEAT_TASK_PRIORITY 1

#define QUEUE_SIZE  10
#define HALF_SECOND_MS  500
#define HEART_BEAT_ON_MS 10
#define HEART_BEAT_OFF_MS 990

#define DEBUG_MSG 0

#define NUM_VALORS_ACC 10
#define NUM_VALORS_LLUM 2
#define NUM_VALORS_TEMP 60

// UART Configuration Parameter (9600 bps - clock 8MHz)
const eUSCI_UART_Config uartConfig = {
EUSCI_A_UART_CLOCKSOURCE_SMCLK,          // SMCLK Clock Source
        8,                                      // BRDIV = 78
        11,                                       // UCxBRF = 2
        0,                                       // UCxBRS = 0
        EUSCI_A_UART_NO_PARITY,                  // No Parity
        EUSCI_A_UART_LSB_FIRST,                  // LSB First
        EUSCI_A_UART_ONE_STOP_BIT,               // One stop bit
        EUSCI_A_UART_MODE,                       // UART mode
        EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION  // Oversampling
        };

// Prototipos de funciones privadas
static void prvSetupHardware(void);
static void prvTempLightWriterTask(void *pvParameters);
static void prvAccWriterTask(void *pvParameters);
static void prvReaderTask(void *pvParameters);
static void prvHeartBeatTask(void *pvParameters);
static void initStaticLCDText();

// Declaracion de un mutex para acceso unico a I2C, UART y GTemp
SemaphoreHandle_t xMutexI2C;
SemaphoreHandle_t xMutexUART;
SemaphoreHandle_t xMutexGTemp;
// Declaracion de un semaforo binario para la lectura de buffer
volatile SemaphoreHandle_t xBinarySemaphore;

typedef enum Sensor
{
    light = 1, temp = 2
} Sensor;

typedef struct
{
    Sensor sensor;
    float value;
} temp_light_t;

QueueHandle_t xQueueTempLigth;

typedef struct
{
    float acc_x;
    float acc_y;
    float acc_z;
} acc_t;

QueueHandle_t xQueueAcc;

float globalTemp;
float lastMinutTempDiff;

int32_t leftTextPosition = 2;
int32_t rigthTextPosition = 77;
int32_t centeredTextPosition = 64;

/* Graphic library context */
Graphics_Context g_sContext;

acc_t ultimsValorsAcc[NUM_VALORS_ACC];
int indexValorsAcc = 0;

temp_light_t ultimsValorsLlum[NUM_VALORS_LLUM];
int indexValorsLlum = 0;

temp_light_t ultimsValorsTemp[NUM_VALORS_TEMP];
int indexValorsTemp = 0;

//funcion imprimir string por UART
void printf_(uint32_t moduleInstance, char *message)
{
    int index = 0;
    while (message[index] != '\0')
    {
        MAP_UART_transmitData(EUSCI_A0_BASE, message[index]);
        index++;
    }
}

int main(void)
{
    // Inicializacion de semaforo binario
    xBinarySemaphore = xSemaphoreCreateBinary();

    // Inicializacio de mutexs
    xMutexI2C = xSemaphoreCreateMutex();
    xMutexUART = xSemaphoreCreateMutex();
    xMutexGTemp = xSemaphoreCreateMutex();

    xQueueTempLigth = xQueueCreate(QUEUE_SIZE, sizeof(temp_light_t));
    xQueueAcc = xQueueCreate(QUEUE_SIZE, sizeof(acc_t));

    // Comprueba si semaforo y mutex se han creado bien
    if ((xBinarySemaphore != NULL) && (xMutexI2C != NULL)
            && (xMutexUART != NULL) && (xMutexGTemp != NULL)
            && (xQueueTempLigth != NULL) && (xQueueAcc != NULL))
    {
        // Inicializacion del hardware (clocks, GPIOs, IRQs)
        prvSetupHardware();

        for (int i = 0; i < NUM_VALORS_ACC; i++)
        {
            ultimsValorsAcc[i].acc_x = -50;
            ultimsValorsAcc[i].acc_y = -50;
            ultimsValorsAcc[i].acc_z = -50;
        }
        for (int i = 0; i < NUM_VALORS_LLUM; i++)
        {
            ultimsValorsLlum[i].sensor = light;
            ultimsValorsLlum[i].value = -1;
        }
        for (int i = 0; i < NUM_VALORS_TEMP; i++)
        {
            ultimsValorsTemp[i].sensor = temp;
            ultimsValorsTemp[i].value = -500;
        }

        globalTemp = 20;

        // Creacion de tareas
        xTaskCreate(prvTempLightWriterTask, "TempLightWriterTask",
        configMINIMAL_STACK_SIZE,
                    NULL,
                    WRITER_TASK_PRIORITY,
                    NULL);
        xTaskCreate(prvAccWriterTask, "AccWriterTask", configMINIMAL_STACK_SIZE,
        NULL,
                    WRITER_TASK_PRIORITY,
                    NULL);
        xTaskCreate(prvReaderTask, "ReaderTask", configMINIMAL_STACK_SIZE, NULL,
        READER_TASK_PRIORITY,
                    NULL);
        xTaskCreate(prvHeartBeatTask, "HeartBeatTask", configMINIMAL_STACK_SIZE,
        NULL,
                    HEARTBEAT_TASK_PRIORITY,
                    NULL);
        // Puesta en marcha de las tareas creadas
        printf_(EUSCI_A0_BASE, "Tareas creadas \n\r");
        vTaskStartScheduler();
    }
    // Solo llega aqui si no hay suficiente memoria
    // para iniciar el scheduler
    return 0;
}

// Inicializacion del hardware del sistema
static void prvSetupHardware(void)
{

    extern void FPU_enableModule(void);

    // Configuracion del pin P1.0 - LED 1 - como salida y puesta en OFF
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0);

    // Inicializacion de pins sobrantes para reducir consumo
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P2, PIN_ALL8);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PB, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PC, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PD, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PE, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P2, PIN_ALL8);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PB, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PC, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PD, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PE, PIN_ALL16);

    // Habilita la FPU
    MAP_FPU_enableModule();
    // Cambia el numero de "wait states" del controlador de Flash
    MAP_FlashCtl_setWaitState(FLASH_BANK0, 2);
    MAP_FlashCtl_setWaitState(FLASH_BANK1, 2);

    // Selecciona la frecuencia central de un rango de frecuencias del DCO
    MAP_CS_setDCOCenteredFrequency(CS_DCO_FREQUENCY_6);
    // Configura la frecuencia del DCO
    CS_setDCOFrequency(CS_8MHZ);

    // Inicializa los clocks HSMCLK, SMCLK, MCLK y ACLK
    MAP_CS_initClockSignal(CS_HSMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_ACLK, CS_REFOCLK_SELECT, CS_CLOCK_DIVIDER_1);

    // Selecciona el nivel de tension del core
    MAP_PCM_setCoreVoltageLevel(PCM_VCORE0);

    // Inicializacion del I2C
    I2C_init();
    // Inicializacion del sensor TMP006
    TMP006_init();
    // Inicializacion del sensor opt3001
    sensorOpt3001Init();
    sensorOpt3001Enable(true);

    // Configuracion del pin P1.1 como entrada con R de pull-up
    MAP_GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P1, GPIO_PIN1);

    // Seleccion de modo UART en pines P1.2 y P1.3
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(
            GPIO_PORT_P1, GPIO_PIN2 | GPIO_PIN3, GPIO_PRIMARY_MODULE_FUNCTION);
    // Configuracion de la UART
    MAP_UART_initModule(EUSCI_A0_BASE, &uartConfig);
    // Habilitacion de la UART
    MAP_UART_enableModule(EUSCI_A0_BASE);

    // Inicializa acelerometro+ADC
    init_Accel();

    // Reset del flag de interrupcion del pin P1.1
    MAP_GPIO_clearInterruptFlag(GPIO_PORT_P1, GPIO_PIN1);
    // Habilita la interrupcion del pin P1.1
    MAP_GPIO_enableInterrupt(GPIO_PORT_P1, GPIO_PIN1);
    // Configura la prioridad de la interrupcion del PORT1
    MAP_Interrupt_setPriority(INT_PORT1, 0xA0);
    // Habilita la interrupcion del PORT1
    MAP_Interrupt_enableInterrupt(INT_PORT1);
    // Habilita que el procesador responda a las interrupciones
    MAP_Interrupt_enableMaster();

    /* Initializes display */
    Crystalfontz128x128_Init();

    /* Set default screen orientation */
    Crystalfontz128x128_SetOrientation(LCD_ORIENTATION_UP);

    /* Initializes graphics context */
    Graphics_initContext(&g_sContext, &g_sCrystalfontz128x128,
                         &g_sCrystalfontz128x128_funcs);
    Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_RED);
    Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
    GrContextFontSet(&g_sContext, &g_sFontFixed6x8);

    initStaticLCDText();
}

//Tarea heart beat
static void prvHeartBeatTask(void *pvParameters)
{
    for (;;)
    {
        MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);
        vTaskDelay(pdMS_TO_TICKS(HEART_BEAT_ON_MS));
        MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);
        vTaskDelay(pdMS_TO_TICKS(HEART_BEAT_OFF_MS));
    }
}

//Tarea lectura temperatura y luz
static void prvTempLightWriterTask(void *pvParameters)
{

    temp_light_t queueRegister;
    uint16_t rawData;
    float convertedLux;
    float temperature;

    int loopCount = 0;

    float sumatoriLlum;
    uint8_t countValorsLlum;

    float sumatoriTemp;

    for (;;)
    {

        //////////////////////////
        // ESPERA DE 500ms
        //////////////////////////
        vTaskDelay(pdMS_TO_TICKS(HALF_SECOND_MS));

        ///////////////////////////////////////////
        // Llegir sensor de llum
        ///////////////////////////////////////////
        if (xSemaphoreTake(xMutexI2C, portMAX_DELAY))
        {
            sensorOpt3001Read(&rawData);
            sensorOpt3001Convert(rawData, &convertedLux);
            xSemaphoreGive(xMutexI2C);
        }
        queueRegister.sensor = light;
        queueRegister.value = convertedLux;

        // AFEGIM EL VALOR A LA CUA CIRCULAR
        ultimsValorsLlum[indexValorsLlum++] = queueRegister;
        if (indexValorsLlum % NUM_VALORS_LLUM == 0)
        {
            indexValorsLlum = 0;
        }

        // DEBUG MSG x UART
        if (DEBUG_MSG && xSemaphoreTake(xMutexUART, portMAX_DELAY))
        {
            printf_(EUSCI_A0_BASE, "Enviando luz... \n");
            xSemaphoreGive(xMutexUART);
        }

        // CALCULAR MITJANA DELS VALOR DE LA CUA CIRCULAR
        sumatoriLlum = 0;
        countValorsLlum = 0;
        for (int i = 0; i < NUM_VALORS_LLUM; i++)
        {
            if (ultimsValorsLlum[i].value != -1)
            {
                sumatoriLlum += ultimsValorsLlum[i].value;
                countValorsLlum++;
            }
        }
        queueRegister.value = sumatoriLlum / countValorsLlum;

        // ENVIAR LA MITJANA CALCULADA A LA CUA FREERTOS
        xQueueSend(xQueueTempLigth, &queueRegister, portMAX_DELAY);

        // CADA SEGON
        if (++loopCount % 2 == 0)
        {
            loopCount = 0;

            ///////////////////////////////////////////
            // Llegir sensor de temperatura
            //////////////////////////////////////////
            if (xSemaphoreTake(xMutexI2C, portMAX_DELAY))
            {
                temperature = TMP006_readAmbientTemperature();
                xSemaphoreGive(xMutexI2C);
            }
            queueRegister.sensor = temp;
            queueRegister.value = temperature;

            // AFEGIM EL VALOR A LA CUA CIRCULAR
            ultimsValorsTemp[indexValorsTemp++] = queueRegister;
            // ENTREM A FER EL CALCUL, SI HI HAN 60 MOSTRES A LA CUA CIRCULAR
            if (indexValorsTemp % NUM_VALORS_TEMP == 0)
            {
                indexValorsTemp = 0;

                // DEBUG MSG x UART
                if (DEBUG_MSG && xSemaphoreTake(xMutexUART, portMAX_DELAY))
                {
                    printf_(EUSCI_A0_BASE, "Enviando temperatura... \n");
                    xSemaphoreGive(xMutexUART);
                }

                // CALCULAR MITJANA DELS VALORS DE LA CUA CIRCULAR
                sumatoriTemp = 0;
                for (int i = 0; i < NUM_VALORS_TEMP; i++)
                {
                    sumatoriTemp += ultimsValorsTemp[i].value;
                }
                queueRegister.value = sumatoriTemp / NUM_VALORS_TEMP;

                // ENVIAR LA MITJANA CALCULADA A LA CUA FREERTOS
                xQueueSend(xQueueTempLigth, &queueRegister, portMAX_DELAY);
            }
        }

    }
}

//Tarea lectura acc
static void prvAccWriterTask(void *pvParameters)
{
    acc_t queue_element;
    float Datos[NUM_ADC_CHANNELS];
    float localTemp;

    if (xSemaphoreTake(xMutexGTemp, portMAX_DELAY))
    {
        localTemp = globalTemp;
        xSemaphoreGive(xMutexGTemp);
    }
    int loopCounter = 0;

    for (;;)
    {

        //////////////////////////
        // ESPERA DE 100ms
        //////////////////////////
        vTaskDelay(pdMS_TO_TICKS(100));

        if (++loopCounter % 300 == 0)
        {
            loopCounter = 0;
            if (xSemaphoreTake(xMutexGTemp, portMAX_DELAY))
            {
                localTemp = globalTemp;
                xSemaphoreGive(xMutexGTemp);
            }
        }

        // SINCRONITZACIO PER SEMAFOR
        xSemaphoreTake(xBinarySemaphore, 0);

        // LLEGIM DEL DADES
        if (xSemaphoreTake(xMutexI2C, portMAX_DELAY))
        {
            Accel_read(Datos);
            xSemaphoreGive(xMutexI2C);
        }
        queue_element.acc_x = Datos[0];
        queue_element.acc_y = Datos[1];
        queue_element.acc_z = Datos[2];

        // AFEGIM EL VALOR A LA CUA CIRCULAR
        ultimsValorsAcc[indexValorsAcc++] = queue_element;
        if (indexValorsAcc % NUM_VALORS_ACC == 0)
        {
            indexValorsAcc = 0;
        }

        // DEBUG MSG x UART
        if (DEBUG_MSG && xSemaphoreTake(xMutexUART, portMAX_DELAY))
        {
            printf_(EUSCI_A0_BASE, "Enviando Aceleracion... \n");
            xSemaphoreGive(xMutexUART);
        }

        // CALCULAR MITJANA DELS VALORS DE LA CUA CIRCULAR
        float sumatoriX = 0;
        float sumatoriY = 0;
        float sumatoriZ = 0;
        uint8_t countValors = 0;
        for (int i = 0; i < NUM_VALORS_ACC; i++)
        {
            if (ultimsValorsAcc[i].acc_x != -50)
            {
                sumatoriX += ultimsValorsAcc[i].acc_x;
                sumatoriY += ultimsValorsAcc[i].acc_y;
                sumatoriZ += ultimsValorsAcc[i].acc_z;
                countValors++;
            }
        }
        queue_element.acc_x = sumatoriX / countValors;
        queue_element.acc_y = sumatoriY / countValors;
        queue_element.acc_z = sumatoriZ / countValors;

        // ENVIAR LA MITJANA CALCULADA A LA CUA FREERTOS
        xQueueSend(xQueueAcc, &queue_element, portMAX_DELAY);

    }
}

//Tarea lectura de cola
static void prvReaderTask(void *pvParameters)
{
    temp_light_t queueRegisterTempLight;
    acc_t queueRegisterAcc;
    char message[50];
    char value[10];

    for (;;)
    {
        ///////////////////////////////////////
        // LLEGIM LA CUA DE ACCELERACIONS
        ///////////////////////////////////////
        // SI ESPEREM MES DE 50ms SALTEM EL PROCESS
        if (xQueueReceive(xQueueAcc, &queueRegisterAcc,
                pdMS_TO_TICKS(50)) == pdTRUE)
        {

            strcpy(message, "\n\rLectura acelerometro:\nX: ");
            ftoaAcc(queueRegisterAcc.acc_x, value, 2);
            Graphics_drawString(&g_sContext, (int8_t *) value,
            AUTO_STRING_LENGTH,
                                rigthTextPosition, 90, OPAQUE_TEXT);
            strcat(message, value);
            strcat(message, "\nY: ");

            ftoaAcc(queueRegisterAcc.acc_y, value, 2);
            Graphics_drawString(&g_sContext, (int8_t *) value,
            AUTO_STRING_LENGTH,
                                rigthTextPosition, 100, OPAQUE_TEXT);
            strcat(message, value);
            strcat(message, "\nZ: ");

            ftoaAcc(queueRegisterAcc.acc_z, value, 2);
            Graphics_drawString(&g_sContext, (int8_t *) value,
            AUTO_STRING_LENGTH,
                                rigthTextPosition, 110, OPAQUE_TEXT);
            strcat(message, value);

            if (xSemaphoreTake(xMutexUART, portMAX_DELAY))
            {
                printf_(EUSCI_A0_BASE, message);
                xSemaphoreGive(xMutexUART);
            }
        }

        ///////////////////////////////////////
        // LLEGIM LA CUA DE TEMP / LLUM
        ///////////////////////////////////////
        // SI ESPEREM MES DE 50ms SALTEM EL PROCESS
        if (xQueueReceive(xQueueTempLigth, &queueRegisterTempLight,
                pdMS_TO_TICKS(50)) == pdTRUE)
        {

            ftoaAcc(queueRegisterTempLight.value, value, 2);
            if (queueRegisterTempLight.sensor == light)
            {
                strcpy(message, "\n\rLectura de luz: ");

                Graphics_drawString(&g_sContext, (int8_t *) value,
                AUTO_STRING_LENGTH,
                                    rigthTextPosition - 5, 65, OPAQUE_TEXT);
            }
            else if (queueRegisterTempLight.sensor == temp)
            {
                if (xSemaphoreTake(xMutexGTemp, portMAX_DELAY))
                {
                    lastMinutTempDiff = queueRegisterTempLight.value
                            - globalTemp;
                    globalTemp = queueRegisterTempLight.value;
                    xSemaphoreGive(xMutexGTemp);
                }

                strcpy(message, "\n\rLectura de temperatura: ");
                Graphics_drawString(&g_sContext, (int8_t *) value,
                AUTO_STRING_LENGTH,
                                    rigthTextPosition, 30, OPAQUE_TEXT);

                ftoaAcc(lastMinutTempDiff, value, 2);
                Graphics_drawString(&g_sContext, (int8_t *) value,
                AUTO_STRING_LENGTH,
                                    rigthTextPosition, 40, OPAQUE_TEXT);
            }
            strcat(message, value);

            if (xSemaphoreTake(xMutexUART, portMAX_DELAY))
            {
                printf_(EUSCI_A0_BASE, message);
                xSemaphoreGive(xMutexUART);
            }
        }

    }
}

static void initStaticLCDText()
{
    int32_t initLine = 5;
    Graphics_clearDisplay(&g_sContext);

    // TITOL
    Graphics_drawStringCentered(&g_sContext, (int8_t *) "SE - PRAC - VMOLLET",
    AUTO_STRING_LENGTH,
                                centeredTextPosition, initLine, OPAQUE_TEXT);

    // TITLO TEMP
    initLine += 15;
    Graphics_drawStringCentered(&g_sContext, (int8_t *) "Temperatura (C)",
    AUTO_STRING_LENGTH,
                                centeredTextPosition, initLine, OPAQUE_TEXT);
    // TEMP MITJA
    initLine += 10;
    Graphics_drawString(&g_sContext, (int8_t *) "Mitja (60s):",
    AUTO_STRING_LENGTH,
                        leftTextPosition, initLine, OPAQUE_TEXT);

    // TEMP DIFF
    initLine += 10;
    Graphics_drawString(&g_sContext, (int8_t *) "Diferencia:",
    AUTO_STRING_LENGTH,
                        leftTextPosition, initLine, OPAQUE_TEXT);

    // TITOL LLUM
    initLine += 15;
    Graphics_drawStringCentered(&g_sContext, (int8_t *) "Llumositat (lux)",
    AUTO_STRING_LENGTH,
                                centeredTextPosition, initLine,
                                OPAQUE_TEXT);

    // LLUM MITJA
    initLine += 10;
    Graphics_drawString(&g_sContext, (int8_t *) "Mitja:",
    AUTO_STRING_LENGTH,
                        leftTextPosition, initLine, OPAQUE_TEXT);

    // TITOL ACC
    initLine += 15;
    Graphics_drawStringCentered(&g_sContext, (int8_t *) "Acceleracio (g)",
    AUTO_STRING_LENGTH,
                                centeredTextPosition, initLine,
                                OPAQUE_TEXT);

    // ACC X MITJA
    initLine += 10;
    Graphics_drawString(&g_sContext, (int8_t *) "Mitja Eix X:",
    AUTO_STRING_LENGTH,
                        leftTextPosition, initLine, OPAQUE_TEXT);

    // ACC Y MITJA
    initLine += 10;
    Graphics_drawString(&g_sContext, (int8_t *) "Mitja Eix Y:",
    AUTO_STRING_LENGTH,
                        leftTextPosition, initLine, OPAQUE_TEXT);

    // ACC Z MITJA
    initLine += 10;
    Graphics_drawString(&g_sContext, (int8_t *) "Mitja Eix Z:",
    AUTO_STRING_LENGTH,
                        leftTextPosition, initLine, OPAQUE_TEXT);
}
